//Sir i have created new api from mocky yet the body of api is same because sir api provided by you was incomplete there was a missing curly bracket at the end of the file you can check here https://run.mocky.io/v3/d53400a3-6126-495e-9d16-0b4414b537b3


fetch('https://run.mocky.io/v3/f32b5a74-fc6e-4c9e-9488-387dbb58ee31')
.then(resp=>resp.json())
.then(data=>
    {
        //here for the first iteration of loop the id variable will be empty because for our first row of table we have given id to all column without any number for example:clientid
        //after first iteration id value will be same as value of i so that it will match with our given column id's 
        //i.e:client1
        for(var i=0;i<3;i++)
        {
            if(i==0)
            var id="";
            else
            var id=i;
            document.getElementById(`clientid${id}`).innerHTML=data.clients[i].id;
            document.getElementById(`closingagent${id}`).innerHTML=data.clients[i].name;
            document.getElementById(`companyname${id}`).innerHTML=data.clients[i].company;
            document.getElementById(`orderId${id}`).innerHTML=data.clients[i].orderId;
            document.getElementById(`invoicepaid${id}`).innerHTML=`$${data.clients[i].invoicepaid}`;
            document.getElementById(`invoicepending${id}`).innerHTML=`$${data.clients[i].invoicePending}`;
        }
    })
.catch(error=>console.log(error))
